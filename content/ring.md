+++
title = "ring"
+++

friends, inspiration & communities on the web:

* [aomame](https://aomame.neocities.org)
* [arcaik](https://arcaik.net)
* [dcat](https://lyngvaer.no)
* [dustri](https://dustri.org)
* [eyenx](https://eyenx.ch)
* [iotek](https://iotek.org)
* [mort](https://mort.coffee)
* [neeasade](https://neeasade.net)
* [nixers](https://nixers.net)
* [ols](https://ols.wtf)
* [pyratebeard](https://pyratebeard.net)
* [jee](https://rtz.dev)
* [thugcrowd](https://thugcrowd.com)
* [movq](https://www.uninformativ.de)
* [venam](https://venam.nixers.net)
* [xero](https://xero.nu)
* [z3bra](https://z3bra.org)
